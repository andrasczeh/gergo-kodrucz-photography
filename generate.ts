import {
    promises as fs
} from 'fs';
import SrcSetGenerator, { SupportedExtension } from '@flexis/srcset';
import Vinyl from 'vinyl';

const inputDir = 'pictures';
const libDir = 'libs';
const outDir = libDir + '/images';
async function generate() {
    const imageList: Card[] = [];
    await createDir(libDir);
    await createDir(outDir);
    const path = inputDir;
    const dirs = await fs.readdir(path);
    console.debug(dirs);
    await asyncForEach(dirs, async dir => {
        const currentPath = path + '/' + dir;
        await createDir(outDir + '/' + dir);
        const pics = await fs.readdir(currentPath);
        console.debug(pics);
        await asyncForEach(pics, async pic => {
            const images = await transformImage(path, dir, pic);
            imageList.push(images);
        })
    })
    console.log();
    await fs.writeFile('libs/cards.json', JSON.stringify(imageList, null, 2));

}

async function transformImage(mainPath: string, category: string, pic: string) {
    const path = mainPath + '/' + category + '/' + pic;
    const contents = await fs.readFile(path);
    const source = new Vinyl({
        path,
        contents
    });
    const srcSet = new SrcSetGenerator();
    const width = [1920, 540, 320];
    const format: SupportedExtension[] = ['webp', 'jpg'];
    const images = srcSet.generate(source, {
        width,
        format,
    });
    let url: string = '';
    const urls: string[] = [];
    for await (const image of images) {
        image.base = './static'; // TODO
        const imageOutputPath = image.path.replace(inputDir, outDir);
        const isMaxWidth = (image.metadata?.width === width[0] || !width.includes(image.metadata?.width as number));
        if (isMaxWidth && image.metadata?.format === 'jpeg') {
            url = imageOutputPath;
        }
        urls.push(encodeURI(`${imageOutputPath}`) +  ` ${image.metadata?.width}w`);
        await fs.writeFile(imageOutputPath, image.contents as any);
    }

    const imageList: Card = {
        title: '',
        category,
        url,
        urls: urls.join(', ')
    };
    return imageList;
}

async function createDir(currentPath: string) {
    try {
        await fs.mkdir(currentPath);
    } catch (error) {
        console.debug(currentPath + ' already exists.');
    }
}

async function asyncForEach(array: Array<any>, callback: (...args: any) => any) {
    for (let index = 0; index < array.length; index++) {
      await callback(array[index], index, array);
    }
  }

generate();

interface Card {
    title: string;
    category: string;
    url: string;
    urls: string;
}