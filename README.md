# Gergo Kodrucz Photography - Website Source

## Képfeltöltés

1. Tegyük a feltöltendő képeket a pictures/demo mappába. Fontos, hogy a fileok elnevezése ".jpg"-re végződjön

2. src/gallery-cards.yml fileban vegyük fel a megfelelő kategóriá(k)ba a képet, "title"-be csak a file ".jpg" előtti nevét írjuk

3. Ha új kategóriát is szeretnénk látrehozni, vegyük fel az src/gallery-categories.yml fileba

4. Ha VSCode-ban csináltuk a változtatásokat, a "Source Control" tabon a hozzáadott/változott fileokon nyomjuk meg a "+" gombot

5. A megfelelő fileok átkerülnek "Staged Changes"-be, írjuk be mit változtattunk, majd Commit (pipa / ctrl+enter)

6. A változtatásokat a "... / Push" gombokkal tudjuk Gitlabra küldeni

## Futtatás saját gépen

1. Telepítsük a következőket:

  - [Git](https://git-scm.com/)

  - [NodeJS](https://nodejs.org/en/)

  - [LiveServer](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer)

2. VSCode terminalból futtassuk a következő parancsokat (2. lépést ha változnak a képek mindig újra kell futtatni): 

  1. npm install

  2. npm run generate-images

3. Jobbkattintás a fileok között az index.html fileon / open in Live server
