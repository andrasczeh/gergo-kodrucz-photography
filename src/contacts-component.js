export const contactComponent = {
    data: {
        contacts: [
            {icon: 'fab fa-facebook-f', prefix: '', value: 'https://www.facebook.com/gkodrucz/'},
            {icon: 'fab fa-instagram', prefix: '', value: 'https://www.instagram.com/gergokodruczphotography/'},
            {icon: 'fas fa-at', prefix: 'mailto:', value: 'gergokodruczphotography@gmail.com'},
            {icon: 'fas fa-phone', prefix: 'tel:', value: '+447549381573'},
        ]
    },
};
