import { GalleryCategories } from './gallery-categories.js';

const Child = {
    template: '#childarea'
};

export const galleryComponent = {
    el: '#gallery',
    data: {
        categories: GalleryCategories,
        cards: [],
        images: [],
        allCards: [],
        isShowing: false,
        bkClass: 'bk',
        blurClass: 'blur',
        index: null,
        hasHidden: false,
        cardsToShow: 3,
        currentCategory: null,
    },
    created: async function () {
        const cards = await (await fetch('./libs/cards.json')).json();
        this.cards = cards;
        this.images = cards;
        this.allCards = [...cards];
        this.filterCategories(this.categories[0]);
    },
    methods: {
        filterCategories: function (category) {
            if (!category) {
                this.cards = [...this.allCards];
                return;
            }
            this.currentCategory = category;
            const cards = this.allCards.filter((c) => c.category === category.label);
            this.hasHidden = this.cardsToShow < cards.length;
            this.cards = cards.splice(0, this.cardsToShow);
        },
        toggleShow() {
            this.isShowing = !this.isShowing;
        },
        onImageLoad(event, card) {
            const rect = event.target.getBoundingClientRect();
            if (rect.height > rect.width) {
                Vue.set(card, 'class', 'max-width-7');
            }
        },
        showMore() {
            this.cardsToShow += 6;
            this.filterCategories(this.currentCategory);
        },
    },
    components: {
      appChild: Child,
    }
};
