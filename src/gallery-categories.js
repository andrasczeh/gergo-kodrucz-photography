export const GalleryCategories = [
    {name: 'Portrait/Business Portrait', label:'portraits'},
    {name: 'Food', label:'food'},
    {name: 'Commercial', label:'commercial'},
    {name: 'Weddings', label:'weddings'},
    {name: 'Engagement', label:'engagement'},
    {name: 'Sports', label:'sports'},
    {name: 'Art', label:'art'},
    {name: 'Streetphoto', label:'streetphoto'},
    {name: 'Landscape', label:'landscape'},
    {name: 'Family and Babyphoto', label:'familyandbabyphoto'},
    {name: 'Animals', label:'animals'},
];
