import { contactComponent } from './contacts-component.js';
import { galleryComponent } from './gallery-component.js';
import { navComponent } from './nav-component.js';

window.toggle = (id) => {
    document.getElementById(id).classList.toggle("show");
}
  
window.onload =  () => {
    new Vue(navComponent);
    Vue.use(VueLightGallery);
    new Vue(galleryComponent);
    new Vue(contactComponent).$mount('#contacts');
    new Vue(contactComponent).$mount('footer');
    window.onclick = function(event) {
        if (!event.target.matches('.noclose')) {
          var dropdowns = document.getElementsByClassName("menu-dropdown");
          var i;
          for (i = 0; i < dropdowns.length; i++) {
            var openDropdown = dropdowns[i];
            if (openDropdown.classList.contains('show')) {
              openDropdown.classList.remove('show');
            }
          }
        }
      }
}