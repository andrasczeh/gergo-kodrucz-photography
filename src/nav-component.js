export const navComponent = {
    el: '#nav',
    data: {
        classes: {
            home: '',
            gallery: '',
            about_me: '',
            contacts: '',
        }
    },
    /* mounted: function () {
        this.$nextTick(function () {
            const footer = document.querySelector('footer');
            const elements = Object.keys(this.classes).map(el => document.querySelector('#' + el));
            const keys = Object.keys(this.classes);
            document.addEventListener('scroll', (ev) => {
                keys.forEach(k => this.classes[k] = '');
                const visibleElements = elements.map(el => this.isScrolledIntoView(el));
                this.classes[keys[visibleElements.lastIndexOf(true)]] = 'active';
                if (this.classes.home === 'active') {
                    this.$el.classList.remove('half-bg');
                    footer.classList.remove('half-bg');
                } else {
                    this.$el.classList.add('half-bg');
                    footer.classList.add('half-bg');
                }
            });

        });
    }, */
    methods: {
        // https://stackoverflow.com/a/488073/643514
        isScrolledIntoView(el) {
            const rect = el.getBoundingClientRect();
            const elemTop = rect.top;
            const elemBottom = rect.bottom;

            // Only completely visible elements return true:
            // var isVisible = (elemTop >= 0) && (elemBottom <= window.innerHeight);
            // Partially visible elements return true:
            const isVisible = elemTop < window.innerHeight && elemBottom >= 0;
            return isVisible;
        }
    }
};
